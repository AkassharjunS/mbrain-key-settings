# mbrain-key-settings

Key | Values
------------ | -------------
VIBRATION | True or False (Boolean)
SOUND | True or False (Boolean)
DELETE SEQUENCE | True or False (Boolean)
PHONETIC SINHALA DISPLAY | True or False (Boolean)
HIDE NUMBER ROW | True or False (Boolean)
SIZE | SMALL, MEDIUM or LARGE (String)
KEYBOARD | WIJESEKARA or PHONETIC (String)
THEME | Theme ID (Integer)

> Both the keys and values (for Strings) are case sensitive.