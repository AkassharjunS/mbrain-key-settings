package com.github.akassharjun.settings;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.RadioButton;
import android.widget.Switch;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;
import me.relex.circleindicator.CircleIndicator2;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainActivity extends AppCompatActivity {

    ThemeAdapter adapter;
    private RecyclerView mThemeList;
    private CircleIndicator2 mIndicator;
    private ConstraintLayout mThemesLayout;
    private RadioButton mSmallRadioButton;
    private RadioButton mMediumRadioButton;
    private RadioButton mLargeRadioButton;
    private RadioButton mWijesekaraRadioButton;
    private RadioButton mPhoneticRadioButton;
    private Switch mSoundSwitch;
    private Switch mVibrationSwitch;

    private String MY_PREFS_NAME = "mBrainKey.prefs";
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Switch mDeleteSequenceSwitch;
    private Switch mPhoneticSinhalaKeyDisplaySwitch;
    private Switch mHideNumberRowSwitch;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();

        sharedPreferences = getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        editor = sharedPreferences.edit();


        // set up the RecyclerView
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
        mThemeList.setLayoutManager(linearLayoutManager);

        adapter = new ThemeAdapter(this, generateImageUrls());
        mThemeList.setAdapter(adapter);

        initSettings();


        PagerSnapHelper pagerSnapHelper = new PagerSnapHelper();
        pagerSnapHelper.attachToRecyclerView(mThemeList);

        mIndicator = findViewById(R.id.indicator);
        mIndicator.attachToRecyclerView(mThemeList, pagerSnapHelper);

        adapter.registerAdapterDataObserver(mIndicator.getAdapterDataObserver());

        mVibrationSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> editor.putBoolean("VIBRATION", isChecked));
        mSoundSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> editor.putBoolean("SOUND", isChecked));

        mSmallRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                editor.putString("SIZE", "SMALL");
            }
        });

        mMediumRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                editor.putString("SIZE", "MEDIUM");
            }
        });

        mLargeRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                editor.putString("SIZE", "LARGE");
            }
        });

        mWijesekaraRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                editor.putString("KEYBOARD", "WIJESEKARA");
            }
        });

        mPhoneticRadioButton.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                editor.putString("KEYBOARD", "PHONETIC");
            }
        });

        mDeleteSequenceSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> editor.putBoolean("DELETE SEQUENCE", isChecked));
        mPhoneticSinhalaKeyDisplaySwitch.setOnCheckedChangeListener((buttonView, isChecked) -> editor.putBoolean("PHONETIC SINHALA DISPLAY", isChecked));
        mHideNumberRowSwitch.setOnCheckedChangeListener((buttonView, isChecked) -> editor.putBoolean("HIDE NUMBER ROW", isChecked));
    }

    private List<String> generateImageUrls() {
        ArrayList<String> imageUrls = new ArrayList<>();
        imageUrls.add("https://i.imgur.com/INR88Ra.png");
        imageUrls.add("https://i.imgur.com/INR88Ra.png");
        imageUrls.add("https://i.imgur.com/INR88Ra.png");
        imageUrls.add("https://i.imgur.com/INR88Ra.png");

        return imageUrls;
    }

    private void initView() {
        mThemeList = findViewById(R.id.themeList);
        mIndicator = findViewById(R.id.indicator);
        mThemesLayout = findViewById(R.id.themesLayout);
        mSmallRadioButton = findViewById(R.id.smallRadioButton);
        mMediumRadioButton = findViewById(R.id.mediumRadioButton);
        mLargeRadioButton = findViewById(R.id.largeRadioButton);
        mWijesekaraRadioButton = findViewById(R.id.wijesekaraRadioButton);
        mPhoneticRadioButton = findViewById(R.id.phoneticRadioButton);
        mSoundSwitch = findViewById(R.id.soundSwitch);
        mVibrationSwitch = findViewById(R.id.vibrationSwitch);
        mDeleteSequenceSwitch = (Switch) findViewById(R.id.deleteSequenceSwitch);
        mPhoneticSinhalaKeyDisplaySwitch = (Switch) findViewById(R.id.phoneticSinhalaKeyDisplaySwitch);
        mHideNumberRowSwitch = (Switch) findViewById(R.id.hideNumberRowSwitch);
    }

    private void initSettings() {
        // booleans
        boolean isVibrationOn = sharedPreferences.getBoolean("VIBRATION", false);
        boolean isSoundOn = sharedPreferences.getBoolean("SOUND", false);
        boolean isDeleteSequenceOn = sharedPreferences.getBoolean("DELETE SEQUENCE", false);
        boolean isPhoneticSinhalaDisplayOn = sharedPreferences.getBoolean("PHONETIC SINHALA DISPLAY", false);
        boolean isHideNumberRowOn = sharedPreferences.getBoolean("HIDE NUMBER ROW", false);
        // strings
        String size = sharedPreferences.getString("SIZE", "");
        String keyboard = sharedPreferences.getString("KEYBOARD", "");
        // integers
        int themeID = sharedPreferences.getInt("THEME", 1);

        if (isVibrationOn) {
            mVibrationSwitch.setChecked(true);
        }

        if (isSoundOn) {
            mSoundSwitch.setChecked(true);
        }

        if (isDeleteSequenceOn) {
            mDeleteSequenceSwitch.setChecked(true);
        }

        if (isPhoneticSinhalaDisplayOn) {
            mPhoneticSinhalaKeyDisplaySwitch.setChecked(true);
        }

        if (isHideNumberRowOn) {
            mHideNumberRowSwitch.setChecked(true);
        }

        Objects.requireNonNull(mThemeList.getLayoutManager()).scrollToPosition(themeID);

        assert size != null;
        switch (size) {
            case "LARGE":
                mLargeRadioButton.setChecked(true);
                break;
            case "MEDIUM":
                mMediumRadioButton.setChecked(true);
                break;
            case "SMALL":
                mSmallRadioButton.setChecked(true);
                break;
            default:
                break;
        }

        assert keyboard != null;
        switch (keyboard) {
            case "WIJESEKARA":
                mWijesekaraRadioButton.setChecked(true);
                break;
            case "PHONETIC":
                mPhoneticRadioButton.setChecked(true);
                break;
            default:
                break;
        }
    }

    private void saveRecyclerViewPosition() {
        int currentVisiblePosition = ((LinearLayoutManager) (Objects.requireNonNull(mThemeList.getLayoutManager()))).findFirstVisibleItemPosition();
        editor.putInt("THEME", currentVisiblePosition);
        Log.d("THEME", String.valueOf(currentVisiblePosition));
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        saveRecyclerViewPosition();
        editor.apply();
    }

    @Override
    protected void onStop() {
        super.onStop();
        saveRecyclerViewPosition();
        editor.apply();

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        saveRecyclerViewPosition();
        editor.apply();

    }
}
